<?php
    include_once '../include/settings.php';
?>
<html>

<head>

<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="../css/emprestimos.css">

</head>

<div id = "menu">

<ul class="menu">
    
<li><a href="../php/form_emprestimo.php">Novo Pedido</a></li>
    
<?php
if (isset($_SESSION['USER_LOGIN']) && $_SESSION['USER_LEVEL'] == 5)
    {
    ?>
    <li><a href="../php/listagem_emprestimos.php">Listagem Empréstimos</a></li>
    <li><a href="../php/listagem_pedidos.php">Listagem Pedidos</a></li>
<?php
    }
    ?>
    <?php
    if (isset($_SESSION['USER_LOGIN']))
    {
    ?>
    <li><a href="../include/logout.php">Logout</a></li>
    <?php
    }
        else
        {
    ?>
        <li><a href="login.php">Login</a></li>
        <?php
        }
        ?>

</ul>

</div>

</html>