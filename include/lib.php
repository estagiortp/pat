<?php 

function db_connect() {
    $link = mysqli_connect('localhost', 'UserEmprestimos', 'emprestimos_sql', 'ProjetoEmprestimos');
	if (!$link) {
		echo "Error: Unable to connect to MySQL." . PHP_EOL;
		echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
		exit;
	}
	mysqli_set_charset($link, "UTF8");
    return $link;
}

function db_close() {
    
	return mysqli_close($_SESSION['link']);
}

function db_query($query)
{		
	$result = mysqli_query($_SESSION['link'], $query);
	if(is_bool($result)) {
		if($id = mysqli_insert_id($_SESSION['link'])) {
			// INSERT - Enviar o ID (campo chave) do novo registo criado
			return $id;
		}
		// GERAL - Por defeito envia o valor TRUE ou FALSE gerado da operação realizada
		return $result;
	} elseif($result) { 
        $arrQuery = [];
		// SELECT - Guardar dados num vetor
        while ($row = mysqli_fetch_assoc($result)) {
		$arrQuery[] = $row;
        }
        return $arrQuery;
	}
	// PROBLEMA - Se não entrar em nunhum IF envia 0
	return 0;
}

function email_notification($pedido_por,$data_inicio,$data_fim,$obs_pedido)
{
    $to = 'informatica.porto@rtp.pt';
    $subject = 'Pedido por: ' . $pedido_por;
    $message = 'Pedido por: ' . $pedido_por . '&#013' . 'Data início: ' . $data_inicio . '&#013' . 'Data fim: ' . $data_fim . '&#013' . 'Observações: ' . $obs_pedido;
    mail($to, $subject, $message);
}

function pr($array) {
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

?>