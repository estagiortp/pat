<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="../css/emprestimos.css">
</head>
</html>
<?php

// include "../include/menu.html";
include_once '../include/settings.php';
include_once '../include/menu.php';
include_once '../include/control.php';

//$opcao = 1;
//button action="_self?opc=1" value="todos">
//button action="_self?opc=2" value="proximos">

//if (get isset ... $opcao = $_GET['opc'])
// 3 butoes : 1 - todos, proximos, concluidos
// CASE 1:  $sql = "SELECT idEmprestimo AS id_emprestimo, PedidoPor AS pedido_por, DataInicio AS data_inicio, DataFim AS data_fim FROM Emprestimos";
// Case 2: SQL = ",,,,,,,,"

// O utilizador deve poder escolher o nº de dias que deseja visualizar. Depois, fazer ciclo com <th>. A data inicial é a (data atual - valor introduzido)
//echo '<input type="text" id="n_dias">';

$sql = "SELECT idEquip as id_equip, Descricao as descequi FROM Equipamentos ORDER BY idGrupo";
$res = db_query($sql);

//-----CORES DA TABELA-----
include $SETTINGS['path_site'] . '/include/cores_listagem.php';
//-----CORES DA TABELA-----

//DIAS A SEREM MOSTRADOS
//Devem ser +1 do que o desejado
$dias_tras = 10; //diferença entre dias_mostrar e dias_tras nao pode ser maior que 14????
$dias_mostrar = 30;
//----------------------

$date = new DateTime('now');
$date->modify('-'. $dias_tras . ' day');
$data_hoje = new DateTime('now');
$data_hoje = $data_hoje->format('Y-m-d');

$dias_da_semana = array(
    1=>'S',
    2=>'T',
    3=>'Q',
    4=>'Q',
    5=>'S',
    6=>'S',
    7=>'D'
);

    
     function equips_ocupados($data) 
    {
        $sql = "SELECT DISTINCT EmpEquip.idEquip AS equips, Emprestimos.idEmprestimo as id_emp, Emprestimos.PedidoPor as pedido_por, EmpEquip.devolvido AS devolvido FROM Emprestimos, EmpEquip WHERE Emprestimos.idEmprestimo = EmpEquip.idEmprestimo AND DataInicio <= '$data' AND DataFim >= '$data' ORDER BY equips";
        $res = db_query($sql);
         
        $equips_ocupados = Array();
        $equips = Array();
        $ids_emp = Array();
        $pedido_por = Array();
        $devolvido = Array();
        $i = 0;
        
        foreach($res as $v)
        {
            $equips[$i] = $v['equips'];
            $ids_emp[$i] = $v['id_emp'];
            $pedido_por[$i] = $v['pedido_por'];
            $devolvido[$i] = $v['devolvido'];
            $i++;
        }
        return array('ids_equip' => $equips,'ids_emp' => $ids_emp, 'pedido_por' => $pedido_por, 'devolvido' => $devolvido);
    }

  echo '<TABLE align="center">';
  echo "<TR>";
  $i = 1;
  echo '<td></td>';
  // http://stackoverflow.com/questions/7247259/get-the-year-month-day-from-a-datetime-in-php
  while ($i <= $dias_mostrar)
  {
       $dia = $date->modify('+1 day');
      
       $equips_ocupados = equips_ocupados($dia->format('Y-m-d'));
       $dias_ocupados[$i] = $equips_ocupados['ids_equip'];
       $ids_emp[$i] = $equips_ocupados['ids_emp'];
       $pedido_por[$i] = $equips_ocupados['pedido_por'];
       $devolvido[$i] = $equips_ocupados['devolvido'];
      
      
        $cor = $cor_th;
        if ($dia->format('N') == 6 || $dia->format('N') == 7)
            $cor = $cor_fim_semana;

        if ($i == $dias_tras)   
            $cor = $cor_hoje;
                
          
                    echo "<TD class='dia_th' align='center' style='background:".$cor.";'> " . $dias_da_semana[$dia->format('N')] . "</TD>";

       $i++;
  } 
  echo "</TR>";
  echo "<TH id='th_equip'> Equipamentos </TH>";
  $i = 1;
  $date = new DateTime('now');
  $date->modify('-'. $dias_tras . ' day');
  while ($i <= $dias_mostrar)
  {
       $dia = $date->modify('+1 day');
      
       $equips_ocupados = equips_ocupados($dia->format('Y-m-d'));
       $dias_ocupados[$i] = $equips_ocupados['ids_equip'];
       $ids_emp[$i] = $equips_ocupados['ids_emp'];
       $pedido_por[$i] = $equips_ocupados['pedido_por'];
       $devolvido[$i] = $equips_ocupados['devolvido'];
       
        $cor = $cor_th;
        if ($dia->format('N') == 6 || $dia->format('N') == 7)
            $cor = $cor_fim_semana;

        if ($i == $dias_tras)   
            $cor = $cor_hoje;
                
          
                    echo "<TD class='dia_th' align='center' style='background:".$cor.";'> " . $dia->format('d') . "</TD>";

       $i++;
  } 
    
    // SELECT DISTINCT EmpEquip.idEquip AS equipas FROM Emprestimos, EmpEquip WHERE Emprestimos.idEmprestimo = EmpEquip.idEmprestimo AND DataInicio <= "2015-12-25" AND DataFim >= "2015-12-25" ORDER BY equipas
    //http://php.net/manual/en/function.in-array.php verificar se valor existe no array results. se existir, pintar celula
    foreach ($res as $v)
    {
        echo '<TR id="equip_calendario">';
        echo '<TD>';
        echo $v['descequi'];
            echo '</TD>';
        $i = 1;
        $date = new DateTime('now');
        $date->modify('-'. $dias_tras . ' day');

        while ($i <= $dias_mostrar)
        {
           $dia = $date->modify('+1 day');


          //var_dump($equips);

            $cor = $cor_default;
            if ($dia->format('N') == 6 || $dia->format('N') == 7)
                $cor = $cor_fim_semana;

            if ($i == $dias_tras)   
                $cor = $cor_hoje; //funcao para verificar a cor com que deve ser pintada a celula

            $title="";
            $href ="";
            $colspan = 1;
            $count_cores = 0;
            //pr($dias_ocupados);
            //pr($ids_emp[$i]);
            //pr($dias_ocupados[$i]);
            if (in_array($v['id_equip'],$dias_ocupados[$i])/* && $v['id_equip'] != 0 && $dias_ocupados[$i] != 0*/) //-------------CENAS
            {
                $idx = array_search ($v['id_equip'],$dias_ocupados[$i]);

                /*echo "<pre>";
                print_r($devolvido[$i]);
                echo "</pre>"; //1111111 */
                if ($devolvido[$i][$idx] == 0)
                {
                    $cor = $cor_ocupado;
                    $count_cores++;
                }
                else
                {
                    $cor = $cor_devolvido;
                    $count_cores++;
                }
               
                //$mesmo_emprestimo = true;
                $add = 1;
                /*do {
                
                    if (in_array($v['id_equip'],$dias_ocupados[$i+$add]) && ( $ids_emp[$i+$add][$idx] == $ids_emp[$i][$idx]))
                    {
                        $colspan++;
                        $dia = $date->modify('+1 day');
                        
                    }  
                    else $mesmo_emprestimo = false;
                    
                    $add++;
                }while ($mesmo_emprestimo);*/

                    while (($i+$add) < $dias_mostrar && // Previne sair fora da tabela
                           in_array($v['id_equip'],$dias_ocupados[$i+$add])) // verifica se no dia seguinte existe empréstimo do mesmo equip
                    {
                        /*echo $add;
                        echo "<pre>";
                        print_r($dias_ocupados[$i+$add]);
                        echo "</pre>";*/
                        $colspan++;
                        $dia = $date->modify('+1 day');
                        $add++;
                    }  
                    
                    

                $id_emprestimo = $ids_emp[$i][$idx];
                $title = "TITLE='Empréstimo nº: ".$id_emprestimo . "&#10Pedido por: " . $pedido_por[$i][$idx] ."' ";
                //$href = "<a href ='form_gestao.php?id=" .$ids_emp[$i][$idx] . "' >&nbsp&nbsp&nbsp&nbsp</a>";
                
                $i=$i+$colspan-1;
            }
            if ($cor == $cor_devolvido || $cor == $cor_ocupado)
            {           
                $id_emp = '<small>#' . $id_emprestimo . '</small>';
                $href = "<a class='href_dia' href ='form_gestao.php?id=" .$id_emprestimo . "' >".$id_emp."</a>";
                
            }
            else
            {
                $id_emp = '';
                $href = '';
            }
            
            
            //echo "<TD class='dia' align='center' style='background:".$cor.";'" . $title . "colspan = ".$colspan . "><a href ='form_gestao.php?id=" .$href . "' >".$id_emp."</a></TD>";
            echo "<TD class='dia' align='center' style='background:".$cor.";'" . $title . "colspan = ".$colspan . ">". $href . "</TD>";
            
           $i++;
         }
         echo "</TR>";
      
    }
echo "</TABLE>";


db_close();

?>