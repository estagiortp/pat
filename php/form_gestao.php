<?php
include_once '../include/settings.php';
include_once '../include/menu.php';
include_once '../include/control.php';
?>
<head>
    
<meta charset="UTF-8">
    
<script src="../js/jquery-1.10.2.js"></script>
<link rel="stylesheet" href="../css/jquery-ui.css">
  <script src="../js/jquery-ui.js"></script>
  <script>
  $(function() {
    $( ".datepicker" ).datepicker({ 
    	dateFormat: 'yy-mm-dd', 
    	minDate: 0,
    	showAnim: 'fadeIn'
    });
  });
  </script>
<script language="Javascript" type="text/javascript">
//function do botao editar
function enableCombo(counter)
{
    document.getElementById("data_inicio").disabled = false;
    document.getElementById("data_fim").disabled = false;
    var i = 1;

    while (i <= counter)
    {
        document.getElementById("nome_equip" + i).disabled = false;
        i++;
    }
}

var $equips = Array();
    
$(document).ready(function() {
    
    // coloca numa variável global todos os equips disponíveis
    /*$equips = $.ajax
    ({
       method: "POST",
       url: "return_equips.php",
       async: false,
       dataType: 'json'
    }).responseText;
    
    $equips = $.parseJSON($equips);
    
    // constrói as comboboxes com os equips disponíveis
    var options = [];
    options.push('<option />');
    for (i = 0; i < $equips.length; i++)
    {
        options.push('<option value="'+
                   $equips[i]['idEquip']+ '">'+
                   $equips[i]['descequi']+ '</option>');
    }
    $('select').each(function()
    {
        $(this).html(options);
    });*/


    $('select').change(function()
    {
        // 
        var combo_anterior = $(this).attr('id');
        var selecionado_anterior = $(this).val();
        $('select').each(function()
        {
            if ($(this).attr('id') != combo_anterior)
            {
                var selecionado = $(this).val();
                if(selecionado == selecionado_anterior)
                //$(this).html(options);
                    $(this).val('');
            }
        });
    });
}); 
    
    function validarData()
        {
            var data_inicio = document.getElementById("data_inicio").value;
            var data_fim = document.getElementById("data_fim").value;
            
            //Verifica se a data está vazia
            if (data_inicio == "" || data_fim == "")
                {
                    alert('Por favor introduza uma data válida');
                    return false;
                }
            //Verifica se a data de início é maior que a data de entrega
            dataInicio = new Date(data_inicio);
            dataFim = new Date(data_fim);
            
            if (dataInicio > dataFim)
                {
                    alert('A data de início não pode ser posterior à data de entrega');
                    return false;
                }
            return true;
        }
</script>
</head>

<?php 
//Valores do pedido selecionado e inserido dentro de variaveis

$id_emprestimo = $_GET["id"];

//Verifica se recebeu um id emprestimo. Se não receber, volta para listagem emprestimos
if ($id_emprestimo == "" || !isset($id_emprestimo))
{
    header('Location: listagem_emprestimos.php');
}

$sql = "SELECT PedidoPor AS pedido_por, DataInicio AS data_inicio, DataFim AS data_fim, Observacoes AS obs_pedido FROM Emprestimos WHERE idEmprestimo = $id_emprestimo";
//A query acima vai buscar todos os dados do emprestimo que foi selecionado
$res = db_query($sql);

$pedido_por = $res[0]["pedido_por"];

$data_inicio  = $res[0]["data_inicio"];

$data_fim = $res[0]["data_fim"];

$obs_pedido = $res[0]["obs_pedido"];


echo "<strong>Id Empréstimo: </strong> $id_emprestimo<br>";

echo "<strong>Pedido por:</strong> $pedido_por <br>";

/*echo "<strong>Data início:</strong> $data_inicio <br>";

echo "<strong>Data fim:</strong> $data_fim <br>";*/

echo "<strong>Observações:</strong> $obs_pedido <br>";




$sql = "SELECT GruposEquip.Descricao AS tipo_equip, EmpEquip.idGrupo AS grupo, EmpEquip.id AS idEmpEquip, EmpEquip.idEquip AS id_equip, EmpEquip.Devolvido AS devolvido, EmpEquip.idEmprestimo FROM EmpEquip, GruposEquip WHERE (GruposEquip.idGrupo = EmpEquip.idGrupo) AND EmpEquip.idEmprestimo = $id_emprestimo";
// A query acima retorna o valor do tipo do equipamento, o grupo, o id, o id do equipamento e o devolvido. 
$res = db_query($sql);

echo '<form action="insert_gestao.php" onsubmit = "return validarData() && enableCombo2()" method="POST">';
echo "<input name='id_emp' type='text' value=$id_emprestimo hidden>"; //PARA ENVIAR ID EMPRESTIMO PARA O insert_gestao.php
echo "<td>";
echo "<strong>Data Início</strong>";
echo "<input type='text' value=$data_inicio name='data_inicio' class='datepicker' id='data_inicio' disabled readonly> <br>";
echo "</td>";
echo "<tr>";
echo "<td>";
echo "<strong>Data Fim</strong>";
echo "<input type='text' value=$data_fim name='data_fim' class='datepicker' id='data_fim' disabled readonly><br>";
echo "</td>";
echo "</tr>";
echo "<table border='1'>";
echo "<tr>";
echo "<td>";
echo "<strong>Tipo de Equipamento:</strong></td>";
echo  "<td>";
$counter = 0;
?>

<script>

//function para que quando é inserio o devolvido com check ou nao,
// é-lhe atríbuido o valor de 1 ou 0 e a combobox do nome equip fica enabled
function enableCombo2()
    {
        document.getElementById('data_inicio').disabled = false;
        document.getElementById('data_fim').disabled = false; //Faz enable das datas antes de estas serem enviadas
        counter = document.getElementById("counter").value;
        var i=1;
        while (i<=counter) 
        {
            //Verifica se alguma das comboboxes está vazia
            if (document.getElementById('nome_equip' + i).value == "")
                {
                    alert('Por favor selecione todos os equipamentos');
                    return false;
                }
            else {
                //Faz enable às comboboxes antes do submit
            document.getElementById('nome_equip' + i).disabled=false;  
                //Verifica o valor do devolvido
            if ( dev = document.getElementById("devolvido" + i).checked == true) 
            {
                document.getElementById("devolvido" + i).value=1;
            }
                }
            i++;
        }
        
        return true;
    }   
</script>



<?php        
foreach ($res as $v)
    {
    	$counter++;
    	$row_tipo_equip = $v["tipo_equip"];
        $grupo = $v['grupo'];
    	echo "<span id = $grupo >" . $row_tipo_equip . "</span>";
    	echo '<input type="text" hidden id="id_emp_equip' . $counter . '" name="id_emp_equip'  . $counter . '" value = "' . $v["idEmpEquip"] . '">';
        //Quando ainda nao foi atrubuido o equipamento do emprestimo selecionado, apresenta a combobox enabled para ser escolhido.
        //O valor do devovlvido é 0 e está hidden.
        if ($v['id_equip'] == 0) {
            //echo $row2["grupo"];
    	    //TODO:  antes de gerar combobox, verificar se campo idEquip dif 0
    	    // se igual a 0   ----- include gerar.....
    	    // se diferente de 0 ---- mostrar em TEXTO o equipmanto atribuido DONE

            /*$sql6 = "SELECT idEquip FROM EmpEquip WHERE id =". $row2['idEmpEquip'];
            $con6 = mysqli_connect("localhost","UserEmprestimos","emprestimos_sql","ProjetoEmprestimos");
            $result6 = mysqli_query($con6,$sql6); */
    	    include "gerar_nome_equip.php";
            echo '<input type="checkbox" hidden name="devolvido' . $counter . '" value ="0" checked>';
 
            $id_equip=0;

        }
        //aqui é feito o form gestao com o devolvido incluido
        else
        {
            $sql2 = "SELECT Equipamentos.Descricao AS descequi FROM Equipamentos, EmpEquip WHERE Equipamentos.idGrupo = " . $v['grupo'] .  " AND Equipamentos.idEquip = " . $v['id_equip'];
            //Nesta query, vai se buscar a informação da descrição do equipamento para a combo box 
            //Caso o idgrupo seja igual ao idgrupo do emprestimo e caso o idequip seja igual ao equipamento selecionado no emprestimo
            $res2=db_query($sql2);
            // echo '<input type="text" disabled id="nome_equip' . $counter . '" name="nome_equip' . $counter . '" value = "' . $row7['descequi'] . '">' ; //agr ja funcemina
            $sql3 = "SELECT Equipamentos.Descricao AS descequi, Equipamentos.idEquip AS idEquip FROM Equipamentos WHERE Equipamentos.idGrupo = " . $v['grupo'];
            $res3 = db_query($sql3);
            echo '<select disabled name="nome_equip' . $counter . '" id="nome_equip' . $counter . '">';
            foreach ($res3 as $x) 
            {
                    if ($x['descequi'] == $res2[0]['descequi'])
                    {
                        echo '<option selected="selected" value="' . $x['idEquip'] .'">' . $x['descequi'] . '</option>';
                    }
                    else
                    {
                        echo '<option value="' . $x['idEquip'] .'">' . $x['descequi'] . '</option>';
                    }
                }
            }
            echo "</select>";
            $nomeid = "devolvido" . $counter;
            $id_equip = $v['id_equip'];
            //Mostra o devolvido apenas se o equipamento já tiver sido atribuido
            if ($id_equip != 0) 
            {
            echo '<input type="checkbox" name="'. $nomeid . '" id="'. $nomeid . '" value =' . $v['devolvido'];
            
            if ($v['devolvido'] == "1")
                 echo " checked>Devolvido";
            else
                echo " unchecked>Devolvido";
           // $id_equip = $v['id_equip'];
                }
        echo '<br>';
        }

echo '<input type = "text" id="counter" hidden name="counter" value = ' . $counter . ">";
echo "</td>";
echo "</tr>";
echo "</table>";
echo '<input type="submit" value="Submeter">'; 
echo '<input type = "text" id="idEmprestimo" hidden name="idEmprestimo" value = ' . $id_emprestimo . ">";
echo '</br>';
echo "</form>";
//if ($id_equip != 0) 
    echo '<button id="editar" onclick="enableCombo('.$counter.')">Editar</button>';

?>