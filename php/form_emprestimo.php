<?php 
include_once '../include/settings.php';
include_once '../include/menu.php';
?>

<html>

<head>

	<meta charset="UTF-8">
	<title>Empréstimos</title>

	<link rel="stylesheet" href="../css/jquery-ui.css">
  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/jquery-ui.js"></script>
  <script>
  $(function() {
    $( ".datepicker" ).datepicker({ 
    	dateFormat: 'yy-mm-dd', 
    	minDate: 0,
    	showAnim: 'fadeIn'
    });
  });
  </script>

  <script src="../js/jquery.ui.datepicker-pt.js"></script>

	<script language="Javascript" type="text/javascript">

	Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
	}
	NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    	for(var i = this.length - 1; i >= 0; i--) {
        	if(this[i] && this[i].parentElement) {
            	this[i].parentElement.removeChild(this[i]);
        	}
    	}
	}

    var counter = 1;

    var limit = 10; //LIMITE DE EQUIPAMENTOS POR PEDIDO

    function disableRemove()
    {
    		if (counter == 1)
   	 	 {
    		document.getElementById('btnRemove').disabled = true;
   		 }
  	  else
    	{
    		document.getElementById('btnRemove').disabled = false;
    	}
	}	


    function addInput(divName){


         if (counter == limit)  {

              alert("Atingiu o limite de equipamentos por pedido");

         }
         else {

              var newdiv = document.createElement('div');
              newdiv.setAttribute("id","div"+counter);

            //  newdiv.setAttribute("id", "div" + counter);

			  newdiv.innerHTML = '<?php include "gerar_tipo_equip.php";?>' + '<input type = \"text\" id = \"obs_equip\">';
		
              document.getElementById(divName).appendChild(newdiv); 
	
			  var novoSelect = document.getElementById('tipo_equip');
			  novoSelect.setAttribute("id", "tipo_equip" + counter);
			  novoSelect.setAttribute("name", "tipo_equip" + counter);
			
			  var novaObs = document.getElementById('obs_equip');
			  novaObs.setAttribute("id", "obs_equip" + counter);
			  novaObs.setAttribute("name", "obs_equip" + counter);
              novaObs.setAttribute("placeholder","Ex: Audacity, autonomia...");

              counter++;

              disableRemove();

              //alert(counter);

              document.getElementById('counter').value = counter;

          	}
          	     	
         
    }

    function removeInput(){

    		div2delete = document.getElementById('div' + (counter-1));
    		div2delete.remove();


    		/*var div = document.getElementById(divName);
    		var divAtual = document.getElementById('div' + counter);
    		alert(counter);
    		div..removeChild(divAtual);*/

    		counter--;

    		disableRemove();

    		document.getElementById('counter').value = counter;

    }


    function validarData()
        {
            var data_inicio = document.getElementById("data_inicio").value;
            var data_fim = document.getElementById("data_fim").value;
            
            //Verifica se a data está vazia
            if (data_inicio == "" || data_fim == "")
                {
                    alert('Por favor introduza uma data válida');
                    return false;
                }
            //Verifica se a data de início é maior que a data de entrega
            dataInicio = new Date(data_inicio);
            dataFim = new Date(data_fim);
            
            if (dataInicio > dataFim)
                {
                    alert('A data de início não pode ser posterior à data de entrega');
                    return false;
                }
        }
        
	</script>

</head>

<body>


	<form action="insert_emprestimo.php" onsubmit="return validarData()" method = "POST">


		Pedido Por: <br>
		<input type="text" name="pedido_por" required> <br>
		
		Data de início: <br>
		<input type="text" class="datepicker" name="data_inicio" id="data_inicio" readonly="readonly" required> <br>

		Data de Entrega: <br> 
		<input type="text" class="datepicker" name="data_fim" id="data_fim" readonly="readonly" required> <br>


		<div id = "dynamicInput">

		Tipo de Equipamento: <br>
			
			<?php include "gerar_tipo_equip.php";?>
			<input type = "text" id = "obs_equip" placeholder = "Ex: Audacity, autonomia...">
			<script language="javascript">

			var novoSelect = document.getElementById('tipo_equip');
			novoSelect.setAttribute("id", "tipo_equip0");
			novoSelect.setAttribute("name", "tipo_equip0");

			var novaObs = document.getElementById('obs_equip');
			novaObs.setAttribute("id", "obs_equip0");
			novaObs.setAttribute("name", "obs_equip0");

			</script>
			
			<input type="button" value="+" onClick="addInput('dynamicInput');">
			<input type="button" value="-" onClick="removeInput();" id="btnRemove">
		</div>
		
		<textarea name="obs_pedido"></textarea>

		<input type="text" id="counter" name="counter" hidden> <br>

		<input type="submit" value="Submeter">

		<!-- POST dos ids de grupo para gerar_nome_equip.php -->


	</form>

</body>
</html>